from radix_engine_toolkit import *
from typing import List
import json
from pathlib import Path

def create_multi_transfer_manifest(account: Address, ressource_XRD: Address, ressource_asset: Address,NETWORK_ID: int)->TransactionManifest:
    # These are stokenet test accounts
    target_account_str1: str = "account_tdx_2_12xyryeyamq7m87qlq7aze5r5g7s0q2qg04jks4vwnukqkkddzl8vlc"
    target_account_str2: str = "account_tdx_2_12yhnt3vcfdzdrpdlnhgcrmfh5q5vyv8ltjejjn747qjk8d5xen0yef"

    #input_file = open('C:\\Users\\danie\\PycharmProjects\\RET_Examples\\examples\\multiDrop.json')
    file_path = Path(__file__, '..').resolve()
    input_file = open(file_path.joinpath("multiDrop.json"))

    data = json.load(input_file)

    jsonData = data['drops']

    drop_list = []

    for item in jsonData:
        drop_details = {"wallet": None, "amount": None, "bucket": None}
        drop_details['wallet'] = item['wallet']
        drop_details['amount'] = item['amount']
        drop_details['bucket'] = item['bucket']
        drop_list.append(drop_details)

    total_amount: int = 0
    for item in drop_list:
        total_amount = total_amount + int(item.get('amount'))

    # Attention: fee has to be paid in xrd!
    manifest_builder: ManifestBuilder = ManifestBuilder().call_method(ManifestBuilderAddress.STATIC(account), "lock_fee",
                 [ManifestBuilderValue.DECIMAL_VALUE(Decimal("10"))]).account_withdraw(account, ressource_XRD, Decimal(str(total_amount)))

    # drop ressource may be different
    for item in drop_list:
        amount = item.get('amount')
        wallet = item.get('wallet')
        bucket = item.get('bucket')
        manifest_builder = (manifest_builder.
                            take_from_worktop(ressource_asset, Decimal(amount), ManifestBuilderBucket(wallet))
                            .account_try_deposit_or_abort(Address(wallet), ManifestBuilderBucket(wallet), None))

    manifest: TransactionManifest = manifest_builder.build(NETWORK_ID)

    return manifest