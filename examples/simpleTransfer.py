from radix_engine_toolkit import *

def create_simple_transfer_manifest(source_account: Address, target_account: Address, ressource: Address, amount: str, network_id: int) -> TransactionManifest:
    manifest: TransactionManifest = (
        ManifestBuilder()
        .call_method(ManifestBuilderAddress.STATIC(source_account), "lock_fee", [ManifestBuilderValue.DECIMAL_VALUE(Decimal("10"))])
        .account_withdraw(source_account, ressource, Decimal(amount))
        .take_all_from_worktop(ressource,  ManifestBuilderBucket("bucket1"))
        .account_try_deposit_or_abort(target_account, ManifestBuilderBucket("bucket1"), None)
        .build(network_id)
    )

    return manifest
