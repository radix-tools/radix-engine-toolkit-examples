# to support this work xrd donations are welcome:
# account_tdx_2_129w7u5almus753spheyxhr0sm6f2qz84kf2g4x9tftdusqd5w07d0j
import project_variables
import random
from utility.Gateway import Gateway
from utility.Gateway import API_SUBMIT_PATH
from utility.Wallet import Wallet
from examples.simpleTransfer import *
from examples.multiTransfer import *
import logging
from logging.handlers import TimedRotatingFileHandler

PROJECT_NAME = project_variables.PROJECT

# Logging
FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
LOG_FILE_ROOT = PROJECT_NAME + "_ROOT_" + ".log"
LOG_FILE_DEBUG = PROJECT_NAME + "_DEBUG_" + ".log"
LOG_FILE_INFO = PROJECT_NAME + "_INFO_" + ".log"
log = logging.getLogger(PROJECT_NAME)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s — %(name)s — %(levelname)s — %(message)s",
    handlers=[
        TimedRotatingFileHandler(LOG_FILE_ROOT, when='midnight'),
        logging.StreamHandler()
    ]
)

MNEMONIC = project_variables.MNEMONIC
NETWORK_ID = project_variables.NETWORK_ID
ACCOUNT_INDEX = project_variables.ACCOUNT_INDEX
TRANSACTION_MESSAGE = project_variables.TRANSACTION_MESSAGE


def main() -> None:
    gateway = Gateway(NETWORK_ID)
    CurrentEpoch = gateway.get_epoch()
    if (CurrentEpoch == -1):
        quit

    bw: Wallet = Wallet(MNEMONIC, ACCOUNT_INDEX, NETWORK_ID)

    logging.getLogger(PROJECT_NAME).info("Wallet {}".format(bw.babylonAddress.as_str()))

    address_book: KnownAddresses = known_addresses(NETWORK_ID)
    xrd_ressource: Address = address_book.resource_addresses.xrd


    #manifest: TransactionManifest = create_simple_transfer_manifest(account,
    #                                Address("account_tdx_2_12yhnt3vcfdzdrpdlnhgcrmfh5q5vyv8ltjejjn747qjk8d5xen0yef")
    #                                                                , xrd_ressource, "10", NETWORK_ID);

    # create_multi_transfer_manifest(account, xrd_ressource, asset_ressource, NETWORK_ID)
    manifest: TransactionManifest = create_multi_transfer_manifest(bw.babylonAddress, xrd_ressource, xrd_ressource, NETWORK_ID)

    print(manifest.instructions().as_str())

    header: TransactionHeader = TransactionHeader(
        network_id=NETWORK_ID,
        start_epoch_inclusive=CurrentEpoch-10,
        end_epoch_exclusive=CurrentEpoch+10,
        nonce=random.randint(0, 0xFFFFFFFF),
        notary_public_key = bw.publicKey,
        notary_is_signatory=True,
        tip_percentage=0,
    )

    temp: Message = Message.PLAIN_TEXT(PlainTextMessage("text/plain", MessageContent.STR(TRANSACTION_MESSAGE)))

    transaction: NotarizedTransaction = (
        TransactionBuilder()
        .header(header)
        .manifest(manifest)
        .message(temp)
        .sign_with_private_key(bw.privateKey)
        .notarize_with_private_key(bw.privateKey)
    )


    # Ensure that the examples is statically valid - if the validation fails an
    # exception will be raised.
    transaction.statically_validate(ValidationConfig.default(NETWORK_ID))
    logging.getLogger(PROJECT_NAME).info("Transaction hash: f{}".format(transaction.intent_hash().as_str()))
    transaction_hex = bytearray(transaction.compile()).hex()
    data = json.dumps({"notarized_transaction_hex": transaction_hex})
    gateway.submit_transaction(API_SUBMIT_PATH, data=data)

if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)
    logging.getLogger(PROJECT_NAME).info("start logging")
    main()