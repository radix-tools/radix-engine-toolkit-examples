import requests
from requests.adapters import HTTPAdapter
from requests import Session
from urllib3.util.retry import Retry
import json
import logging

API_SUBMIT_PATH = "transaction/submit"
API_CONSTRUCTION_PATH = "transaction/construction"

class Gateway:
    def __init__(self, network_id: int, URL: str = None):
        if URL is None:
            self.url = 'https://stokenet.radixdlt.com/'
            if (network_id == 1):
                self.url = 'https://mainnet.radixdlt.com/'
        else: 
            # optional for e.g. community gateways
            self.url = URL

        self.session: Session = requests.Session()
        logging.getLogger(str(type(self))).info("Session {}".format(self.session))
    
    def get_epoch(self)->int:
            response = self.submit_transaction(API_CONSTRUCTION_PATH)
            if response!=-1:
                epoch = int(response['ledger_state']['epoch'])
                logging.getLogger(str(type(self))).info("Epoch {}".format(epoch))
                return epoch
            else:
                 return response
         
    def submit_transaction(self,api_path,headers={"Content-Type": "application/json"},data=None)->str:
            logging.getLogger(str(type(self))).info("API path {}".format(api_path))
            try:
                logging.getLogger(str(type(self))).info("Submitting {} {}".format(headers,data))
                response = self.session.post(url=f"{self.url+api_path}", data=data, headers=headers, timeout=5)
            except:
                logging.getLogger(str(type(self))).info("All exception")
                return -1
            if(response.status_code != 200):
                return -1
            logging.getLogger(str(type(self))).info("Response {}".format(response.status_code))
            logging.getLogger(str(type(self))).info("Response text{}".format(json.loads(response.text)))
            raw_data = json.loads(response.text)
            return raw_data
    
            