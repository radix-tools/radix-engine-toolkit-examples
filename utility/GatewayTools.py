import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import json


#
#   retry webscrap
#
def retry_session(retries, session=None, backoff_factor=0.3):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        allowed_methods=None,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session

def SubmitTransaction(Transaction, network_id: int):
    BASE_URL_SUBMIT = 'https://stokenet.radixdlt.com/transaction/submit'
    if (network_id == 0x01):
        BASE_URL_SUBMIT = 'https://mainnet.radixdlt.com/transaction/submit'

    Data_list = {
            "notarized_transaction_hex": Transaction
        }

    headers={
        "Content-Type": "application/json"
        }
    session = retry_session(retries=5)    
    try:
        response = session.post(url=f"{BASE_URL_SUBMIT}", data=json.dumps(Data_list), headers=headers, timeout=5)
    except:
        print('All exception Buy')
        #print (dateTimeObj,'All except 0',file=file)
        return 1,0
    if(response.status_code != 200):
        print('pa status code != 200 ',response.status_code)
        #print (dateTimeObj,'status code != 200',file=file)
        print(json.loads(response.text))
        return 1,0
    print("*********************************************************************************************")
    print("examples sent")
    return 0

def GetEpoch(network_id: int):
    BASE_URL_CONSTRUCTION = 'https://stokenet.radixdlt.com/transaction/construction'
    if (network_id == 0x01):
        BASE_URL_CONSTRUCTION = 'https://mainnet.radixdlt.com/transaction/construction'

    headers={
        "Content-Type": "application/json"
        }
    session = retry_session(retries=5)
    try:
        response = session.post(url=f"{BASE_URL_CONSTRUCTION}", headers=headers, timeout=5)
    except:
        print('All exception Buy')
        #print (dateTimeObj,'All except 0',file=file)
        return -1
    if(response.status_code != 200):
        print('pa status code != 200 ',response.status_code)
        #print (dateTimeObj,'status code != 200',file=file)
        print(json.loads(response.text))
        return -1
    raw_data = json.loads(response.text)
    return int(raw_data['ledger_state']['epoch'])