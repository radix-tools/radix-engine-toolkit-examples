from mnemonic import Mnemonic
from hdwallet import HDWallet
from radix_engine_toolkit import *

class Wallet:

    def __init__(self, seed: str, account_index: int, network_id: int):
        self.seed = seed
        self.account_index = account_index
        self.network_id = network_id

        seed_bytes: bytes = Mnemonic.to_seed(seed)
        """
        Returns the Olympia HDWallet given the seed and the account index from which
        the private and public keys are obtained. This function uses the derivation
        path from Olympia.
        """
        params: dict[str, tuple[int, bool]] = {
            "purpose": (44, True),
            "coinType": (1022, True),
            "account": (0, True),
            "change": (0, False),
        }
        hdwallet: HDWallet = HDWallet()
        hdwallet.from_seed(seed_bytes.hex())
        for _, values_tuple in params.items():
            value, hardened = values_tuple
            hdwallet.from_index(value, hardened)
        hdwallet.from_index(account_index, True)

        private_key: bytearray = bytearray.fromhex(hdwallet.private_key());
        public_key: bytearray = bytearray.fromhex(hdwallet.public_key());

        self.privateKey = PrivateKey.new_secp256k1(list(private_key))
        self.publicKey = PublicKey.SECP256K1(list(public_key))
        self.babylonAddress: Address = derive_virtual_account_address_from_public_key(PublicKey.SECP256K1(list(public_key)), network_id)
        