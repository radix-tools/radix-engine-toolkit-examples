from radix_engine_toolkit import *
from typing import Tuple
from hdwallet import HDWallet

MNEMONIC: str = "bonus call entry parent position spoil deal satoshi elephant put creek review smart scheme upper impulse tuna retire suffer join lobster fiscal flash assault"
# Convert the mnemonic to a seed.

#def create_wallet(seed: bytes, account_index: int) -> HDWallet:
def create_wallet(seed: bytes, account_index: int, network_id: int) -> Tuple[PrivateKey, PublicKey, Address]:
    """
    Returns the Olympia HDWallet given the seed and the account index from which
    the private and public keys are obtained. This function uses the derivation
    path from Olympia.
    """
    params: dict[str, tuple[int, bool]] = {
        "purpose": (44, True),
        "coinType": (1022, True),
        "account": (0, True),
        "change": (0, False),
    }
    hdwallet: HDWallet = HDWallet()
    hdwallet.from_seed(seed.hex())
    for _, values_tuple in params.items():
        value, hardened = values_tuple
        hdwallet.from_index(value, hardened=hardened)
    hdwallet.from_index(account_index, True)

    private_key: bytearray = bytearray.fromhex(hdwallet.private_key());
    public_key: bytearray = bytearray.fromhex(hdwallet.public_key());

    return (PrivateKey.new_secp256k1(list(private_key)), PublicKey.SECP256K1(list(public_key)), babylon_account_address_from_public(public_key, network_id))


def olympia_account_address_from_public(public_key: bytearray, olympia_network: OlympiaNetwork) -> Address:
    """
    Derives the account address associated with the public key on Olympia.
    """
    return derive_olympia_account_address_from_public_key(PublicKey.SECP256K1(list(public_key)), olympia_network)

def olympia_address_to_babylon(olympia_resource_address: OlympiaAddress, network_id: int) -> Address:
    public_key: PublicKey = derive_public_key_from_olympia_account_address(olympia_resource_address)
    return derive_virtual_account_address_from_public_key(public_key, network_id)

def babylon_account_address_from_public(public_key: bytearray, network_id: int) -> Address:
    """
    Derives the account address associated with the public key on Babylon.
    """
    return derive_virtual_account_address_from_public_key(PublicKey.SECP256K1(list(public_key)), network_id)
